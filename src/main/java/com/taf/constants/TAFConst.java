/**
 * Utility Config class should not have public constructors
 * Using for getting Constants throughout the TAF
 */
package com.taf.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.taf.configuration.TAFConfig.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TAFConst {
    public static final String TAF_MAIN_PROPERTIES = "src/main/resources/properties/taf/taf.properties";
    public static final String TAF_ENV_PROPERTIES_PATH = "src/main/resources/properties/env/%s.properties";
    public static final String TAF_ENV = getEnv();
    public static final String TAF_ENV_PROPERTIES = setUpEnvProperties();
    public static final String REPORT_OUTPUT_LOCATION = getProperty(TAF_MAIN_PROPERTIES, "tas.reporter.out");
    public static final String DOWNLOADS_LOCATION = getProperty(TAF_MAIN_PROPERTIES, "tas.downloads");
    public static final String PROJECT_NAME = getProperty(TAF_MAIN_PROPERTIES, "tas.project");
    public static final String SYSTEM_UNDER_TEST = getProperty(TAF_MAIN_PROPERTIES, "tas.reporter.systemUnderTestName");
}
