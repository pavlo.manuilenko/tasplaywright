/**
 * Utility Config class should not have public constructors
 * Using for getting Constants throughout the TAF
 */
package com.taf.constants;

import com.taf.configuration.Browser;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.taf.configuration.TAFConfig.getProperty;
import static com.taf.configuration.TAFConfig.getSystemProperty;
import static com.taf.constants.TAFConst.TAF_ENV_PROPERTIES;
import static com.taf.constants.TAFConst.TAF_MAIN_PROPERTIES;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UIConst {
    public static final boolean TRACING = getSystemProperty("tracing").equalsIgnoreCase("true");
    public static final boolean HEADLESS = getSystemProperty("headless").equalsIgnoreCase("true");
    public static final Browser BROWSER = Browser.valueOfName(getSystemProperty("gui.browser"));
    public static final String DESKTOP_BROWSER_SIZE = getProperty(TAF_MAIN_PROPERTIES, "gui.desktopBrowserSize");
    public static final String ANDROID_BROWSER_SIZE = getProperty(TAF_MAIN_PROPERTIES, "gui.galaxyS8BrowserSize");
    public static final String IOS_BROWSER_SIZE = getProperty(TAF_MAIN_PROPERTIES, "gui.iPhone13ProBrowserSize");
    public static final String IOS_USER_AGENT = getProperty(TAF_MAIN_PROPERTIES, "gui.iPhone13ProUserAgent");
    public static final String ANDROID_USER_AGENT = getProperty(TAF_MAIN_PROPERTIES, "gui.galaxyS8UserAgent");
    public static final String LOCALE = getProperty(TAF_MAIN_PROPERTIES, "gui.locale");
    public static final String TIMEZONE = getProperty(TAF_MAIN_PROPERTIES, "gui.timezone");
    public static final double GEOLOCATION_LATITUDE = Double.parseDouble(getProperty(TAF_MAIN_PROPERTIES, "gui.geolocationLatitude"));
    public static final double GEOLOCATION_LONGITUDE = Double.parseDouble(getProperty(TAF_MAIN_PROPERTIES, "gui.geolocationLongitude"));
    public static final double BROWSER_TIMEOUT = Double.parseDouble(getProperty(TAF_MAIN_PROPERTIES, "gui.browserTimeout"));
    public static final String BASE_UI_URL = getProperty(TAF_ENV_PROPERTIES, "gui.baseUrl");
    public static final String ABOUT_GOOGLE_PAGE_UI_URL = getProperty(TAF_ENV_PROPERTIES, "gui.aboutGooglePageUrl");
    public static final String GUI_TEXTS_DATA = getProperty(TAF_MAIN_PROPERTIES, "gui.texts.data");
    public static final String GUI_IMAGES_PATH = getProperty(TAF_MAIN_PROPERTIES, "gui.images.location");
}
