package com.taf.configuration;

public enum Browser {
    CHROMIUM("chromium", "Chromium"),
    CHROME("chrome", "Google Chrome"),
    SAFARI("safari", "Apple Safari (WebKit)"),
    EDGE("edge", "MS Edge"),
    FIREFOX("firefox", "Firefox (Nightly)"),
    ANDROID("android", "Android emulation (Chromium)"),
    IOS("ios", "iOS emulation (WebKit)");

    private final String description;
    private final String name;

    Browser(String browsersName, String description) {
        this.description = description;
        this.name = browsersName;
    }

    @Override
    public String toString() {
        return description;
    }

    public String getName() {
        return name;
    }

    public static Browser valueOfName(String browserName) {
        for (Browser browser : values()) {
            if (browser.name.equals(browserName)) {
                return browser;
            }
        }
        return Browser.CHROMIUM;
    }
}
