/**
 * Manager of TAF GUI part
 * Utility Config class should not have public constructors
 * Using for get Properties from create required browser instance, managed by Playwright
 */
package com.taf.configuration;

import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Playwright;
import com.microsoft.playwright.Browser;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import static com.taf.constants.UIConst.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Log4j2
public class BrowserManager {
    private static boolean browserInit = false;
    private static String browserSize;
    private static Playwright playwright;

    public static Browser initBrowser() {
        playwright = Playwright.create();
        Browser browser = null;
        switch (BROWSER) {
            case CHROME:
                browserSize = DESKTOP_BROWSER_SIZE;
                browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setChannel("chrome").setHeadless(HEADLESS));
                break;
            case SAFARI:
                browserSize = DESKTOP_BROWSER_SIZE;
                browser = playwright.webkit().launch(new BrowserType.LaunchOptions().setHeadless(HEADLESS));
                break;
            case EDGE:
                browserSize = DESKTOP_BROWSER_SIZE;
                browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setChannel("msedge").setHeadless(HEADLESS));
                break;
            case FIREFOX:
                browserSize = DESKTOP_BROWSER_SIZE;
                browser = playwright.firefox().launch(new BrowserType.LaunchOptions().setHeadless(HEADLESS));
                break;
            case ANDROID:
                browserSize = ANDROID_BROWSER_SIZE;
                browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(HEADLESS));
                break;
            case IOS:
                browserSize = IOS_BROWSER_SIZE;
                browser = playwright.webkit().launch(new BrowserType.LaunchOptions().setHeadless(HEADLESS));
                break;
            default:
                browserSize = DESKTOP_BROWSER_SIZE;
                browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(HEADLESS));
                break;
        }
        browserInit = true;
        return browser;
    }

    public static void closeBrowser() {
        playwright.close();
    }

    public static boolean wasBrowserInitiated() {
        return browserInit;
    }

    public static String getBrowserSize() {
        return browserSize;
    }

    public static int getBrowserWidth() {
        return Integer.parseInt(browserSize.substring(0, browserSize.indexOf("x")));
    }

    public static int getBrowserHeight() {
        return Integer.parseInt(browserSize.substring(browserSize.indexOf("x") + 1));
    }
}
