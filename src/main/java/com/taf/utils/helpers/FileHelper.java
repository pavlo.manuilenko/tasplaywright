/**
 * Utility Helper class should not have public constructors
 * Used to read and write data from files
 * If more extensions will be added better to move them into separate Enum class
 */
package com.taf.utils.helpers;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static com.taf.constants.TAFConst.DOWNLOADS_LOCATION;
import static com.taf.constants.TAFConst.TAF_ENV;
import static com.taf.constants.UIConst.GUI_IMAGES_PATH;
import static com.taf.constants.UIConst.GUI_TEXTS_DATA;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileHelper {
    private static final String TXT = ".txt";

    public static String getStringFromFile(String fileName) {
        Path path = Paths.get(GUI_TEXTS_DATA + fileName + TXT);
        try {
            return Files.readString(path);
        } catch (IOException e) {
            log.error("File is not found {}", path.toString());
            return "";
        }
    }

    public static List<String> getStringListFromFile(String fileName) {
        if (TAF_ENV.equals("ci")) {
            fileName = fileName.concat("_ci");
        }
        Path path = Paths.get(GUI_TEXTS_DATA + fileName + TXT);
        try {
            return Files.readAllLines(path);
        } catch (IOException e) {
            log.error("File is not found {}", path.toString());
            return new ArrayList<>(List.of(""));
        }
    }

    public static Path getFileImagePath(String fileName, boolean absolutePath) {
        Path path = Paths.get(GUI_IMAGES_PATH + fileName);
        if (absolutePath) {
            return path.toAbsolutePath();
        }
        return path;
    }

    public static Path downloadFileByURL(String url, boolean renameIMG) {
        String imgFileName = renameIMG ?
                ("downloaded_img" + url.substring(url.lastIndexOf("."))) : url.substring(url.lastIndexOf("/"));
        File file = new File(DOWNLOADS_LOCATION + "images/" + imgFileName);
        try {
            FileUtils.copyURLToFile(URI.create(url).toURL(), file);
        } catch (IOException ioe) {
            log.error("Impossible to download a file by URL {}\n{}", url, ioe);
        }
        return Paths.get(file.getPath());
    }
}
