/**
 * Utility Helper class should not have public constructors
 * Used for tasks related to images
 */

package com.taf.utils.helpers;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.sikuli.script.Finder;
import org.sikuli.script.Screen;

import static com.taf.constants.UIConst.HEADLESS;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ImageHelper {

    /**
     * The method works with Screen object of Sikuli.
     * Screen requires displaying objects as part of view aria, so it will not work in headless mode.
     * Also, multithreading can produce situation when the browser window will be covered with another one,
     * so expected object will not be seen on the screen.
     *
     * @param fileImagePath is an absolute path to the expected image.
     * @return is a boolean parameter (true or false) of attempt to finding the part of screen with expected image
     */
    public static boolean isImageVisibleOnTheScreen(String fileImagePath) {
        if (HEADLESS) {
            log.warn("The browser running in headless mode, so Sikuli will not be able to find the image!");
            return false;
        } else {
            Screen screen = new Screen();
            try {
                screen.wait(fileImagePath);
                return true;
            } catch (Exception e) {
                log.error("[Sikuli] Image was not found (expected: {})\n{}", fileImagePath, e);
                return false;
            }
        }
    }

    /**
     * The method works with Finder object of Sikuli.
     * The method compares two images.
     *
     * @param expFileImagePath is an absolute path to the expected image.
     * @param actFileImagePath is an absolute path to the actual image.
     * @return is a boolean parameter (true or false) of attempt to compare two images
     */
    public static boolean areImagesSimilar(String expFileImagePath, String actFileImagePath) {
        Finder finder = new Finder(expFileImagePath);
        finder.find(actFileImagePath);
        if (finder.hasNext()) {
            return true;
        }
        finder.remove();
        return false;
    }
}
