package com.taf.layers.api.models.retailer;

import lombok.Getter;

public class RetailerData {
    @Getter
    private Integer id;
    @Getter
    private String name;
    @Getter
    private String location;
    @Getter
    private String license;

    public String getIdFromLicense() {
        return getLicense().substring(getLicense().indexOf("-") + 1,
                getLicense().lastIndexOf("-"));
    }
}
