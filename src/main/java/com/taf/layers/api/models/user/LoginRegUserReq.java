package com.taf.layers.api.models.user;

import lombok.Getter;

public class LoginRegUserReq {
    public LoginRegUserReq(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public LoginRegUserReq(String email) {
        this.email = email;
    }

    @Getter
    private String email;

    @Getter
    private String password;

}
