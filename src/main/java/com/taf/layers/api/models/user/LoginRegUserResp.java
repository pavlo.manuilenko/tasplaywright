package com.taf.layers.api.models.user;

import lombok.Getter;

public class LoginRegUserResp {
    @Getter
    private Integer id;

    @Getter
    private String token;

    @Getter
    private String error;

}