/**
 * Java Net HTTP Client class for API testing, which implements RestClient interface
 * Not determinative, public methods return the object, because the Method Chaining pattern is applied
 */
package com.taf.layers.api;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.GsonJsonProvider;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class JavaNetClient {
    private HttpResponse<String> response;
    private final Map<String, String> headers = new TreeMap<>();
    private final Map<String, String> parameters = new TreeMap<>();

    public JavaNetClient setRequestParam(String key, String value) {
        this.parameters.put(key, value);
        return this;
    }

    public JavaNetClient setRequestParams(Map<String, String> queryParams) {
        this.parameters.putAll(queryParams);
        return this;
    }

    public JavaNetClient setRequestHeader(String key, String value) {
        this.headers.put(key, value);
        return this;
    }

    public JavaNetClient setRequestHeaders(Map<String, String> headers) {
        this.headers.putAll(headers);
        return this;
    }

    private String getRequestParamsAsString() {
        if (parameters.isEmpty()) {
            return "";
        } else if (parameters.size() > 1) {
            StringBuilder params = new StringBuilder("?");
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                params.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
            return params.deleteCharAt(params.length() - 1).toString();
        } else {
            Map.Entry<String, String> entry = parameters.entrySet().iterator().next();
            return "?" + entry.getKey() + "=" + entry.getValue();
        }
    }

    private void sendRequest(String method, String endpoint, String contentType, Object obj) {
        HttpRequest.Builder requestBuilder = HttpRequest.newBuilder();
        if (contentType != null) {
            this.headers.put("Content-Type", contentType);
        }
        headers.forEach(requestBuilder::header);
        try {
            log.info(String.format("Sending %s request to %s%s", method, endpoint, getRequestParamsAsString()));
            switch (method) {
                case "GET":
                    response = HttpClient.newHttpClient().send(requestBuilder
                            .uri(new URI(endpoint + getRequestParamsAsString()))
                            .GET()
                            .build(), HttpResponse.BodyHandlers.ofString());

                    break;
                case "POST":
                    response = HttpClient.newHttpClient().send(requestBuilder
                            .uri(new URI(endpoint + getRequestParamsAsString()))
                            .POST(HttpRequest.BodyPublishers.ofString(new Gson().toJson(obj)))
                            .build(), HttpResponse.BodyHandlers.ofString());

                    break;
                case "PUT":
                    response = HttpClient.newHttpClient().send(requestBuilder
                            .uri(new URI(endpoint + getRequestParamsAsString()))
                            .PUT(HttpRequest.BodyPublishers.ofString(new Gson().toJson(obj)))
                            .build(), HttpResponse.BodyHandlers.ofString());

                    break;
                case "DELETE":
                    response = HttpClient.newHttpClient().send(requestBuilder
                            .uri(new URI(endpoint + getRequestParamsAsString()))
                            .DELETE()
                            .build(), HttpResponse.BodyHandlers.ofString());

                    break;
                default:
                    response = HttpClient.newHttpClient().send(requestBuilder
                            .uri(new URI(endpoint + getRequestParamsAsString()))
                            .method(method, HttpRequest.BodyPublishers.ofString(new Gson().toJson(obj)))
                            .build(), HttpResponse.BodyHandlers.ofString());

            }
        } catch (URISyntaxException | IOException e) {
            log.error("Bad URL or IOException during " + method + " request!" + e.getMessage());
        } catch (InterruptedException ie) {
            log.error("The " + method + "  request was interrupted!\n" + ie.getMessage());
            Thread.currentThread().interrupt();
        } finally {
            headers.clear();
        }
    }

    public JavaNetClient sendGetRequest(String endpoint, String contentType) {
        sendRequest("GET", endpoint, contentType, null);
        return this;
    }

    public JavaNetClient sendPostRequest(String endpoint, String contentType, Object obj) {
        sendRequest("POST", endpoint, contentType, obj);
        return this;
    }

    public JavaNetClient sendPutRequest(String endpoint, String contentType, Object obj) {
        sendRequest("PUT", endpoint, contentType, obj);
        return this;
    }

    public JavaNetClient sendPatchRequest(String endpoint, String contentType, Object obj) {
        sendRequest("PATCH", endpoint, contentType, obj);
        return this;
    }

    public JavaNetClient sendDeleteRequest(String endpoint) {
        sendRequest("DELETE", endpoint, null, null);
        return this;
    }

    public int getStatusCode() {
        return response.statusCode();
    }

    public String getResponseBodyAsString() {
        return response.body();
    }

    public <T> T getResponseAsObject(@NonNull String jsonPath, Class<T> obj) {
        if (!jsonPath.isEmpty() && !jsonPath.equals(".")) {
            String json = getJsonStringForPath(response.body(), jsonPath);
            return new Gson().fromJson(json.substring(1, json.length() - 1), obj);
        }
        return new Gson().fromJson(response.body(), obj);
    }

    public <T> List<T> getResponseAsObjectsList(@NonNull String jsonPath, Class<T> obj) {
        if (!jsonPath.isEmpty() && !jsonPath.equals(".")) {
            String json = getJsonStringForPath(response.body(), jsonPath);
            return new Gson().fromJson(json.substring(1, json.length() - 1), TypeToken.getParameterized(List.class, obj).getType());
        }
        return new Gson().fromJson(response.body(), TypeToken.getParameterized(List.class, obj).getType());
    }

    private String getJsonStringForPath(String jsonAsString, String jsonPath) {
        Configuration jsonPathConfig = Configuration.builder().jsonProvider(new GsonJsonProvider())
                .options(Option.ALWAYS_RETURN_LIST, Option.SUPPRESS_EXCEPTIONS).build();
        return JsonPath.using(jsonPathConfig).parse(jsonAsString).read(jsonPath).toString();
    }

}
