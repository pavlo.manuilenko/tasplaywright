package com.taf.layers.gui.components;

import com.microsoft.playwright.FileChooser;
import com.microsoft.playwright.Page;
import com.taf.utils.helpers.FileHelper;

public class SearchByIMGModalWindow {
    Page page;
    private static final String UPLOAD_BUTTON = "xpath=//span[text()='upload a file  ']";

    public SearchByIMGModalWindow(Page page) {
        this.page = page;
    }

    public void uploadFile(String fileName) {
        FileChooser fileChooser = page.waitForFileChooser(() -> page.querySelector(UPLOAD_BUTTON).click());
        fileChooser.setFiles(FileHelper.getFileImagePath(fileName, false));
    }
}
