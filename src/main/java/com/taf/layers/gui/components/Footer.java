package com.taf.layers.gui.components;

import com.microsoft.playwright.Page;
import com.taf.layers.gui.pages.AboutGooglePage;
import com.taf.utils.helpers.FileHelper;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;


public class Footer {
    private final Page page;
    @Getter
    private final List<String> footerItems = FileHelper.getStringListFromFile("footer_items_en_texts");
    @Getter
    private final List<String> mobFooterItems = FileHelper.getStringListFromFile("mob_footer_items_en_texts");
    private static final String FOOTER_ELEMENTS_LOCATOR = "xpath=//a[@class='pHiOh']";
    private static final String FOOTER_ELEMENTS_LOCATOR_MOB_VIEW = "xpath=//a[@class='Fx4vi']";
    private static final String NEGATIVE_MODAL_BUTTON = "No, thanks";

    public Footer(Page page) {
        this.page = page;
    }

    public int getFooterElementsCount() {
        return page.querySelectorAll(FOOTER_ELEMENTS_LOCATOR).size();
    }

    public int getMobFooterElementsCount() {
        return page.querySelectorAll(FOOTER_ELEMENTS_LOCATOR_MOB_VIEW).size();
    }

    public List<String> getFooterElementsNames(boolean isMobile) {
        if (isMobile) {
            return page.querySelectorAll(FOOTER_ELEMENTS_LOCATOR_MOB_VIEW)
                    .stream().map(eh -> eh.innerText().trim()).collect(Collectors.toList());
        } else {
            return page.querySelectorAll(FOOTER_ELEMENTS_LOCATOR)
                    .stream().map(eh -> eh.innerText().trim()).collect(Collectors.toList());
        }
    }

    public AboutGooglePage openAboutGooglePage(Page page, boolean isMobile) {
        if (isMobile) {
            if (!page.getByText(NEGATIVE_MODAL_BUTTON).isHidden()) {
                page.getByText(NEGATIVE_MODAL_BUTTON).tap();
            }
            page.querySelectorAll(FOOTER_ELEMENTS_LOCATOR_MOB_VIEW).get(6).tap();
        } else {
            page.querySelectorAll(FOOTER_ELEMENTS_LOCATOR).get(0).click();
        }
        page.waitForURL(AboutGooglePage.EXP_PAGE_URL + "*");
        return new AboutGooglePage(page);
    }

}
