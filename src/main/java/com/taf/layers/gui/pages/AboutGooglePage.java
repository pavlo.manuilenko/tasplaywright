package com.taf.layers.gui.pages;

import com.microsoft.playwright.ElementHandle;
import com.microsoft.playwright.Page;
import com.taf.utils.helpers.FileHelper;

public class AboutGooglePage {
    private final Page page;
    public static final String EXP_PAGE_URL = "https://about.google/";
    public static final String EXP_TITLE_TEXT = FileHelper.getStringFromFile("about_google_page_title_en_text");
    public static final String EXP_HEADER_TEXT = FileHelper.getStringFromFile("about_google_page_header_en_text");
    private static final String HEADER_LOCATOR = "xpath=//h1";
    private static final String TITLE_TEXT = "xpath=//title[text()]";

    public AboutGooglePage(Page page) {
        this.page = page;
    }

    public void navigate(String address) {
        page.navigate(address);
    }

    public String getExpURL() {
        return EXP_PAGE_URL;
    }

    public ElementHandle getTitle() {
        return page.querySelector(TITLE_TEXT);
    }

    public ElementHandle getHeaderFromPage() {
        return page.querySelector(HEADER_LOCATOR);
    }

    public String getPageAddress() {
        return page.url().substring(0, page.url().indexOf('?'));
    }

    public String getHeaderText() {
        return getHeaderFromPage().innerText().trim();
    }

    public String getExpectedHeaderText() {
        return AboutGooglePage.EXP_HEADER_TEXT;
    }

    public String getExpTitleText() {
        return AboutGooglePage.EXP_TITLE_TEXT;
    }

}
