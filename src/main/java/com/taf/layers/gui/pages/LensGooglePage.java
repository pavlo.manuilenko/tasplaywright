package com.taf.layers.gui.pages;

import com.microsoft.playwright.Page;
import com.taf.utils.helpers.FileHelper;
import com.taf.utils.helpers.ImageHelper;

import java.nio.file.Path;

public class LensGooglePage {
    private final Page page;
    public static final String EXP_PAGE_URL = "https://lens.google.com/";
    private static final String RESULT_SEARCH_HEADER = "xpath=//h2[@class='DeMn2d']";
    private static final String FIRST_IMG_IN_SEARCH_RESULT = "xpath=//a[@aria-label='Image #1 for Googleplex']";

    public LensGooglePage(Page page) {
        this.page = page;
    }

    public String getResultSearchHeaderText() {
        return page.querySelector(RESULT_SEARCH_HEADER).innerText();
    }

    public boolean isImagePresentOnThePage(String fileImageName) {
        return ImageHelper.isImageVisibleOnTheScreen(FileHelper.getFileImagePath(fileImageName, true).toString());
    }

    public boolean isExpImageSimilarToDownloadedImageFromThePage(String expectedFileImageName, Path downloadedImagePath) {
        return ImageHelper.areImagesSimilar(FileHelper
                        .getFileImagePath(expectedFileImageName, true).toString(),
                downloadedImagePath.toAbsolutePath().toString());
    }

    public Path downloadFirstSearchResultImage(boolean renameIMG) {
        return FileHelper.downloadFileByURL(page.querySelector(FIRST_IMG_IN_SEARCH_RESULT)
                .getAttribute("href"), renameIMG);
    }
}
