package com.taf.layers.gui.pages;

import com.microsoft.playwright.Page;
import com.taf.layers.gui.components.Footer;
import com.taf.layers.gui.components.SearchByIMGModalWindow;
import lombok.Getter;

import java.util.List;

import static com.taf.configuration.extentreports.ExtentReportsManager.showMethodInvocationInReportDetails;

public class MainPage {
    private final Page page;
    private static final String GOOGLE_LOGO = "xpath=//img[@class='lnXdpd' or @id='hplogo']";
    private static final String SEARCH_FIELD = "xpath=//*[@name='q']";
    private static final String SEARCH_BY_IMG_BUTTON = "xpath=//*[@aria-label='Search by image']";
    @Getter
    private final Footer footer; //Composition of the footer aria element
    @Getter
    private final SearchByIMGModalWindow searchByIMGModalWindow; //Composition of the Search by image Modal window

    public MainPage(Page page) {
        this.page = page;
        footer = new Footer(this.page);
        searchByIMGModalWindow = new SearchByIMGModalWindow(this.page);
    }

    public void navigate(String address) {
        page.navigate(address);
    }

    public String getCurrentUrl() {
        return page.url();
    }

    public String getTitleText() {
        return page.title();
    }

    public boolean isGoogleLogoPresent() {
        return page.isVisible(GOOGLE_LOGO);
    }

    public boolean isGoogleSearchFieldPresent() {
        return page.isEditable(SEARCH_FIELD);
    }

    public int getMainPageFooterElementsCount(boolean isMobileView) {
        if (isMobileView) {
            return getFooter().getMobFooterElementsCount();
        } else {
            return getFooter().getFooterElementsCount();
        }
    }

    public List<String> getFooterItems(boolean isMobileView) {
        if (isMobileView) {
            return getFooter().getMobFooterItems();
        } else {
            return getFooter().getFooterItems();
        }
    }

    public AboutGooglePage openAboutGooglePageFromFooter(boolean isMobileView) {
        showMethodInvocationInReportDetails(true);
        return footer.openAboutGooglePage(page, isMobileView);
    }

    public void openSearchByImageModalWindow() {
        page.click(SEARCH_BY_IMG_BUTTON);
    }

    public LensGooglePage uploadImageIntoSearchByImageModalWindow(String fileImageName) {
        getSearchByIMGModalWindow().uploadFile(fileImageName);
        page.waitForURL(LensGooglePage.EXP_PAGE_URL + "*");
        return new LensGooglePage(page);
    }
}
