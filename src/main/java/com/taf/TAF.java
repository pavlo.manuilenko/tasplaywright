/**
 * Facade Design Pattern include all method through pages and models to work with each page and API of AUT
 */
package com.taf;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Tracing;
import com.microsoft.playwright.options.Geolocation;
import com.taf.layers.api.JavaNetClient;
import com.taf.layers.api.ServerInterceptorCalls;
import com.taf.layers.gui.pages.AboutGooglePage;
import com.taf.layers.gui.pages.LensGooglePage;
import com.taf.layers.gui.pages.MainPage;
import lombok.Getter;
import lombok.Setter;

import java.nio.file.Paths;
import java.util.List;

import static com.taf.configuration.Browser.ANDROID;
import static com.taf.configuration.Browser.IOS;
import static com.taf.configuration.BrowserManager.*;
import static com.taf.constants.TAFConst.REPORT_OUTPUT_LOCATION;
import static com.taf.constants.UIConst.*;

public class TAF {
    @Getter
    private JavaNetClient restClient;
    @Getter
    private ServerInterceptorCalls interceptor;
    @Getter
    private boolean mobileView = false;
    private Browser browser;
    private BrowserContext context;
    @Getter
    private MainPage mainPage;
    @Getter
    @Setter
    private AboutGooglePage aboutGooglePage;
    @Getter
    @Setter
    private LensGooglePage lensGooglePage;

    /**
     * @param isGUI               initiate a browser instance if TAF requested for a base GUI test class.
     *                            If a base API test class requested for a TAF no need to instantiate extra resource,
     *                            only JavaNetClient will be provided for a base API test class.
     * @param interceptorAPICalls passed as Singleton instance for a base mocked APIs test class.
     */
    public TAF(boolean isGUI, ServerInterceptorCalls interceptorAPICalls) {
        if (isGUI) {
            browser = initBrowser();
        } else {
            restClient = new JavaNetClient();
            interceptor = interceptorAPICalls;
        }
    }

    public void quit() {
        closeBrowser();
    }

    public void closeContext() {
        context.close();
    }

    /**
     * A new browser context and a page instantiated for each test method to prevent them from influencing each other.
     * setDefaultTimeout setting will change the default maximum time (30000 mile second) for all the methods accepting BROWSER_TIMEOUT from taf.properties
     * setDefaultNavigationTimeout setting will change the default maximum navigation time for Page.navigate(), Page.reload(), etc.
     */
    public Page getNewPage() {
        if (BROWSER.equals(ANDROID)) {
            mobileView = true;
            context = browser.newContext(new Browser.NewContextOptions()
                    .setHasTouch(true)
                    .setIsMobile(true)
                    .setViewportSize(getBrowserWidth(), getBrowserHeight())
                    .setDeviceScaleFactor(3)
                    .setUserAgent(ANDROID_USER_AGENT)
                    .setGeolocation(new Geolocation(GEOLOCATION_LATITUDE, GEOLOCATION_LONGITUDE))
                    .setPermissions(List.of("geolocation"))
                    .setLocale(LOCALE)
                    .setTimezoneId(TIMEZONE)
            );
        } else if (BROWSER.equals(IOS)) {
            mobileView = true;
            context = browser.newContext(new Browser.NewContextOptions()
                    .setHasTouch(true)
                    .setIsMobile(true)
                    .setViewportSize(getBrowserWidth(), getBrowserHeight())
                    .setDeviceScaleFactor(3)
                    .setUserAgent(IOS_USER_AGENT)
                    .setGeolocation(new Geolocation(GEOLOCATION_LATITUDE, GEOLOCATION_LONGITUDE))
                    .setPermissions(List.of("geolocation"))
                    .setLocale(LOCALE)
                    .setTimezoneId(TIMEZONE)
            );
        } else {
            context = browser.newContext(new Browser.NewContextOptions()
                    .setViewportSize(getBrowserWidth(), getBrowserHeight())
                    .setLocale(LOCALE)
                    .setTimezoneId(TIMEZONE)
            );
        }
        context.setDefaultTimeout(BROWSER_TIMEOUT);
        context.setDefaultNavigationTimeout(BROWSER_TIMEOUT / 5);
        return context.newPage();
    }

    /**
     * Method is collecting Playwright traces if boolean TRACING constant is equal true (system property).
     */
    public void startTracing() {
        if (TRACING) {
            context.tracing().start(new Tracing.StartOptions()
                    .setScreenshots(true)
                    .setSnapshots(true));
        }
    }

    /**
     * Method is saving trace in zip file with TestNG test-id in the name.
     * @param id passes from iTestResult.id()
     * @param saveTrace boolean is true if a test is failed.
     * @return path in String to use in a zip-file link of log event in a report.
     */
    public String stopTracing(String id, boolean saveTrace) {
        String path = "";
        if (TRACING) {
            if (saveTrace) {
                path = String.format("tracing/trace_%s.zip", id);
                context.tracing().stop(new Tracing.StopOptions().setPath(Paths
                        .get(REPORT_OUTPUT_LOCATION.substring(0, REPORT_OUTPUT_LOCATION.lastIndexOf('/') + 1) + path)));
            } else {
                context.tracing().stop();
            }

        }
        return path;
    }

    public void openMainPage(Page page) {
        mainPage = new MainPage(page);
        mainPage.navigate(BASE_UI_URL);
    }

    public void openAboutGooglePage(Page page) {
        aboutGooglePage = new AboutGooglePage(page);
        aboutGooglePage.navigate(ABOUT_GOOGLE_PAGE_UI_URL);
    }

}
