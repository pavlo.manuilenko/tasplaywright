package com.taf.testscenarios.gui.images;

import com.taf.testscenarios.gui.BaseGUITest;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

public class SearchByImagePageTest extends BaseGUITest {
    @Test(suiteName = "SearchByImagePageTests",
            testName = "Search of Google office image by Google logo test",
            description = "Requirements: Task_10",
            groups = {"Regression", "XBrowser"})
    public void searchGoogleOfficeIMGByGoogleLogoTest() {
        getTaf().openMainPage(getPage());
        getTaf().getMainPage().openSearchByImageModalWindow();
        getTaf().setLensGooglePage(getTaf().getMainPage().uploadImageIntoSearchByImageModalWindow("google_office.jpg"));
        Assertions.assertThat(getTaf().getLensGooglePage().getResultSearchHeaderText()).isEqualTo("Googleplex");
        Assertions.assertThat(getTaf().getLensGooglePage()
                .isExpImageSimilarToDownloadedImageFromThePage("Googleplex.jpg",
                        getTaf().getLensGooglePage().downloadFirstSearchResultImage(true))).isTrue();
    }
}
