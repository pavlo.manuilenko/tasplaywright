package com.taf.testscenarios.gui.about;

import com.taf.testscenarios.gui.BaseGUITest;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

public class AboutPageTest extends BaseGUITest {
    @Test(suiteName = "AboutPageTests",
            testName = "Direct open About Google page test",
            description = "Requirements: Task_08",
            groups = {"Regression", "MobileView", "XBrowser"})
    public void openAboutGooglePageTest() {
        getTaf().openAboutGooglePage(getPage());
        Assertions.assertThat(getTaf().getAboutGooglePage().getHeaderText())
                .isEqualTo(getTaf().getAboutGooglePage().getExpectedHeaderText());
    }

    @Test(suiteName = "AboutPageTests",
            testName = "About Google page title text test",
            description = "Requirements: Task_09",
            groups = "Regression")
    public void aboutGooglePageTitleTextTest() {
        getTaf().openAboutGooglePage(getPage());
        Assertions.assertThat(getTaf().getAboutGooglePage().getTitle().innerText())
                .isEqualTo(getTaf().getAboutGooglePage().getExpTitleText());
    }
}
