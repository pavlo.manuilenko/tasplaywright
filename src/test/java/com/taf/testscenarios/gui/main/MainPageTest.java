package com.taf.testscenarios.gui.main;

import com.taf.layers.gui.pages.MainPage;
import com.taf.testscenarios.gui.BaseGUITest;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

import java.util.List;

import static com.taf.constants.UIConst.BASE_UI_URL;

public class MainPageTest extends BaseGUITest {
    private MainPage mainPage;

    @Test(priority = 0,
            suiteName = "MainPageTests",
            testName = "Google Home page address test",
            description = "Requirements: Task_01",
            groups = "Regression")
    public void addressTest() {
        getTaf().openMainPage(getPage());
        Assertions.assertThat(getTaf().getMainPage().getCurrentUrl()).isEqualTo(BASE_UI_URL);
    }

    @Test(priority = 1,
            suiteName = "MainPageTests",
            testName = "Google Home page title text test",
            description = "Requirements: Task_02",
            groups = "Regression")
    public void titleTextTest() {
        getTaf().openMainPage(getPage());
        Assertions.assertThat(getTaf().getMainPage().getTitleText()).isEqualTo("Google");
    }

    @Test(priority = 2,
            suiteName = "MainPageTests",
            testName = "Google Home page Logo test",
            description = "Requirements: Task_03",
            groups = {"Regression", "MobileView"})
    public void googleLogoTest() {
        getTaf().openMainPage(getPage());
        Assertions.assertThat(getTaf().getMainPage().isGoogleLogoPresent()).isTrue();
    }

    @Test(suiteName = "MainPageTests",
            testName = "Google search field available test",
            description = "Requirements: Task_04",
            groups = {"Regression", "MobileView"},
            dependsOnMethods = {"googleLogoTest"})
    public void googleSearchFieldAvailableTest() {
        getTaf().openMainPage(getPage());
        Assertions.assertThat(getTaf().getMainPage().isGoogleSearchFieldPresent()).isTrue();
    }

    @Test(suiteName = "MainPageTests",
            testName = "Google Home page footer elements count test",
            description = "Requirements: Task_05",
            groups = {"XBrowser", "MobileView"},
            dependsOnMethods = {"googleLogoTest"})
    public void googleFooterElementsCountTest() {
        getTaf().openMainPage(getPage());
        if (getTaf().isMobileView()) {
            Assertions.assertThat(getTaf().getMainPage().getMainPageFooterElementsCount(true)).isEqualTo(7);
        } else {
            Assertions.assertThat(getTaf().getMainPage().getMainPageFooterElementsCount(false)).isEqualTo(6);
        }
    }

    @Test(testName = "Google Home page footer elements names test",
            description = "Requirements: Task_06",
            groups = "MobileView",
            dependsOnMethods = {"googleLogoTest"})
    public void googleFooterElementsNamesTest() {
        getTaf().openMainPage(getPage());
        List<String> expectedFooterElementsNames = getTaf().getMainPage().getFooterItems(getTaf().isMobileView());
        List<String> actualFooterElementsNames = getTaf().getMainPage().getFooter().getFooterElementsNames(getTaf().isMobileView());
        Assertions.assertThat(actualFooterElementsNames).isEqualTo(expectedFooterElementsNames);
    }

    @Test(testName = "Open About Google page from Home page footer test",
            description = "Requirements: Task_07",
            groups = "MobileView",
            enabled = false,
            dependsOnMethods = {"googleLogoTest"})
    public void openAboutGooglePageFromMainPageFooterTest() {
        getTaf().openMainPage(getPage());
        getTaf().setAboutGooglePage(getTaf().getMainPage().openAboutGooglePageFromFooter(getTaf().isMobileView()));
        Assertions.assertThat(getTaf().getAboutGooglePage().getExpURL()).isEqualTo(getTaf().getAboutGooglePage().getPageAddress());
    }

}
