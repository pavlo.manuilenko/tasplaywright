package com.taf.testscenarios.gui;

import com.microsoft.playwright.Page;
import com.taf.TAF;
import com.taf.testscenarios.CommonBaseTest;
import lombok.Getter;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static com.taf.configuration.extentreports.ExtentReportsManager.includeTestToReport;

public class BaseGUITest extends CommonBaseTest {
    @Getter
    private TAF taf = new TAF(true, null);
    @Getter
    private Page page;

    @BeforeMethod(alwaysRun = true)
    public void initPageAndReporting(Method method) {
        page = taf.getNewPage();
        taf.startTracing();
        includeTestToReport(method.getAnnotation(Test.class));
    }

    @AfterMethod(alwaysRun = true)
    public void closeContext(ITestResult iTestResult) {
        taf.stopTracing(iTestResult.id(), false);
        taf.closeContext();
    }

    @AfterSuite(alwaysRun = true)
    public void closeBrowser() {
        taf.quit();
    }

}