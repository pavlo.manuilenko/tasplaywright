package com.taf.utils.listeners;

import com.aventstack.extentreports.Status;
import com.microsoft.playwright.Page;
import com.taf.TAF;
import com.taf.configuration.extentreports.ExtentReportsManager;
import com.taf.testscenarios.CommonBaseTest;
import com.taf.testscenarios.gui.BaseGUITest;
import lombok.NonNull;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.util.Base64;

import static com.taf.configuration.extentreports.ExtentReportsManager.getTest;
import static com.taf.constants.UIConst.TRACING;
import static com.taf.utils.loggers.TAFLogger.*;

public class ExtentReportTestListener implements ITestListener {
    private static int passedTests = 0;
    private static int failedTests = 0;
    private static int skippedTests = 0;
    private static int countClassFinished = 0;

    private static String getTestMethodName(@NonNull ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    private static String getTestMethodPage(@NonNull ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    @Override
    public void onStart(@NonNull ITestContext iTestContext) {
        logClassOnStart(iTestContext.getName());
    }

    @Override
    public void onFinish(@NonNull ITestContext iTestContext) {
        logClassOnFinish(iTestContext.getName());
        if (CommonBaseTest.getCountChildClassStarted() == ++countClassFinished) {
            logTotalScenariosStatistic(passedTests, failedTests, skippedTests);
        }
        ExtentReportsManager.extentReports.flush();
    }

    @Override
    public void onTestStart(@NonNull ITestResult iTestResult) {
        logTestMethodStart(getTestMethodName(iTestResult));
    }

    @Override
    public void onTestSuccess(@NonNull ITestResult iTestResult) {
        passedTests++;
        logTestMethodPassed(getTestMethodName(iTestResult));
        getTest().log(Status.PASS, "PASSED");
    }

    /**
     * @param iTestResult <code>ITestResult</code> containing information about the run test
     *                    Increase failed tests counter
     *                    Attach status and throwable data if test located in API package
     *                    Attach status, throwable data and screenshot in Base64 if test located in GUI package
     *                    Get page from BaseTest and assign to local variable.
     *                    Save zip file with trace and add a link to download (instead of throwable data and screenshot) if Playwright tracing is enabled.
     */
    @Override
    public void onTestFailure(@NonNull ITestResult iTestResult) {
        failedTests++;
        logTestMethodFailed(getTestMethodName(iTestResult), iTestResult.getThrowable());
        if (iTestResult.getTestClass().getRealClass().getPackageName().contains(".api.")) {
            getTest().log(Status.FAIL, iTestResult.getThrowable());
        } else {
            Object testClass = iTestResult.getInstance();
            TAF taf = ((BaseGUITest) testClass).getTaf();
            Page page = ((BaseGUITest) testClass).getPage();
            if (TRACING) {
                getTest().log(Status.FAIL, "FAILED - <a href='" + taf
                        .stopTracing(iTestResult.id(), true) + "'><b>Download Playwright trace</b></a>");
            } else {
                getTest().log(Status.FAIL, "FAILED", iTestResult.getThrowable(),
                        getTest().addScreenCaptureFromBase64String(Base64
                                .getEncoder().encodeToString(page.screenshot())).getModel().getMedia().get(0));
            }
        }
    }


    @Override
    public void onTestSkipped(@NonNull ITestResult iTestResult) {
        skippedTests++;
        logTestMethodSkipped(getTestMethodName(iTestResult));
        getTest().log(Status.SKIP, "SKIPPED");
    }

}
