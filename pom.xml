<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>test.automation.solution</groupId>
    <artifactId>TASPlaywright</artifactId>
    <version>1.0</version>
    <description>Test Automation Solution based on Playwright</description>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
        <apache.maven.plugins-version>3.2.5</apache.maven.plugins-version>
        <log4j-version>2.22.1</log4j-version>
        <extentreports-version>5.1.1</extentreports-version>
        <assertj-version>3.25.2</assertj-version>
        <playwright-version>1.41.1</playwright-version>
        <testng-version>7.9.0</testng-version>
        <lombok-version>1.18.30</lombok-version>
        <jayway-version>2.9.0</jayway-version>
        <gson-version>2.10.1</gson-version>
        <wiremock-version>3.3.1</wiremock-version>
        <sikuli-version>2.0.5</sikuli-version>
        <apache-commons-version>3.14.0</apache-commons-version>
        <threadCount>1</threadCount>
        <testSuiteName>all_tests</testSuiteName>
    </properties>

    <dependencies>
        <!-- Maven Surefire Plugin  https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-surefire-plugin -->
        <dependency>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>${apache.maven.plugins-version}</version>
        </dependency>
        <!-- TestNG https://mvnrepository.com/artifact/org.testng/testng -->
        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
            <version>${testng-version}</version>
        </dependency>
        <!-- MS Playwright https://mvnrepository.com/artifact/com.microsoft.playwright/playwright -->
        <dependency>
            <groupId>com.microsoft.playwright</groupId>
            <artifactId>playwright</artifactId>
            <version>${playwright-version}</version>
        </dependency>
        <!-- AssertJ https://mvnrepository.com/artifact/org.assertj/assertj-core -->
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>${assertj-version}</version>
            <scope>test</scope>
        </dependency>
        <!-- ExtentReports https://mvnrepository.com/artifact/com.aventstack/extentreports -->
        <dependency>
            <groupId>com.aventstack</groupId>
            <artifactId>extentreports</artifactId>
            <version>${extentreports-version}</version>
        </dependency>
        <!-- Log4j Core https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core -->
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>${log4j-version}</version>
        </dependency>
        <!-- SLF4J Bridge (to fix Failed to load class org.slf4j.impl.StaticLoggerBinder) https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-slf4j-impl -->
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-slf4j-impl</artifactId>
            <version>${log4j-version}</version>
            <scope>test</scope>
        </dependency>
        <!-- Project Lombok https://mvnrepository.com/artifact/org.projectlombok/lombok -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok-version}</version>
            <scope>provided</scope>
        </dependency>
        <!-- Jayway Jsonpath https://mvnrepository.com/artifact/com.jayway.jsonpath/json-path -->
        <dependency>
            <groupId>com.jayway.jsonpath</groupId>
            <artifactId>json-path</artifactId>
            <version>${jayway-version}</version>
        </dependency>
        <!-- Google Gson https://mvnrepository.com/artifact/com.google.code.gson/gson -->
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>${gson-version}</version>
        </dependency>
        <!-- WireMock https://mvnrepository.com/artifact/org.wiremock/wiremock -->
        <dependency>
            <groupId>org.wiremock</groupId>
            <artifactId>wiremock</artifactId>
            <version>${wiremock-version}</version>
            <scope>compile</scope>
        </dependency>
        <!-- Sikuli https://mvnrepository.com/artifact/com.sikulix/sikulixapi -->
        <dependency>
            <groupId>com.sikulix</groupId>
            <artifactId>sikulixapi</artifactId>
            <version>${sikuli-version}</version>
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-nop</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>log4j</groupId>
                    <artifactId>log4j</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <!-- Apache Lang https://mvnrepository.com/artifact/org.apache.commons/commons-lang3 -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>${apache-commons-version}</version>
        </dependency>
    </dependencies>
    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>${apache.maven.plugins-version}</version>
                    <configuration>
                        <parallel>tests</parallel>
                        <!--passing ass command-line attribute, default: 1-->
                        <threadCount>${threadCount}</threadCount>
                        <suiteXmlFiles>
                            <!--passing ass command-line attribute, default: all_tests-->
                            <suiteXmlFile>
                                src/test/resources/suites/${testSuiteName}.xml
                            </suiteXmlFile>
                        </suiteXmlFiles>
                        <properties>
                            <property>
                                <name>usedefaultlisteners</name>
                                <value>false</value> <!-- disabling default listeners -->
                            </property>
                            <property>
                                <name>ExtentReportTestListener</name>
                                <value>com.taf.utils.listeners.ExtentReportTestListener</value>
                            </property>
                        </properties>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
</project>