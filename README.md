# TestAutomationSolution (Playwright)
This automation testing framework (TAF) is created and maintained by [Pavlo Manuilenko](https://www.linkedin.com/in/manuilenko-pavlo/)

The main purpose of this TAF is to use it as a base (skeleton), which saves a lot of time at the beginning of the project and helps to get the AT up and running quicker.

## Applied technologies (Frameworks)
Java 17, Maven, Playwright, Sikuli, TestNG, AssertJ, Java.net.http, Google Gson, Jayway jsonPath, WireMock, Log4J2, Extent Reports, Lombok.<hr/>

## Table of contents
>1. [Initialization of the TAF](#initialization-of-the-taf)
    <br/>1.1 [Required Software, Tools and Prerequisites](#required-software-tools-and-prerequisites)
    <br/>1.2 [How to build the Gradle project and setup different test runs](#how-to-build-the-gradle-project-and-setup-different-test-runs)
    <br/>1.3 [Local run and debugging](#local-run-and-debugging)
>2. [API testing](#api-testing)
    <br/>2.1 [Mock API calls](#mock-api-calls)
>3. [GUI testing](#gui-testing)
    <br/>3.1 [Image testing](#image-testing)
    <br/>3.2 [Cross-browser testing](#cross-browser-testing)
    <br/>3.3 [Mobile-view testing](#mobile-view-testing)
>4. [Reporting](#reporting)
    <br/>4.1 [Tracing](#tracing)
>5. [CI/CD](#cicd)
    <br/>5.1 [GitLab CI/CD](#continuous-integration-implemented-via-gitlab-cicd)
<hr />

### Initialization of the TAF
This TAF is as a base (skeleton) of usual WEB automation testing.
If it's architecture and technologies meet your primary needs just clone (`git clone https://gitlab.com/pavlo.manuilenko/tasplaywright.git`)
the repo and adapt it under your project (**recommended**).<br>[Fork project](https://gitlab.com/pavlo.manuilenko/tasplaywright/-/forks/new)
is another way to use this TAF for your needs, which allows you to make changes without affecting the original project.<br>

There are some examples of page objects and components under `src/main/java/com/taf/layers/gui`, API models under `src/main/java/com/taf/layers/api`
and tests under `src/test/java/com/taf/testscenarios` which should be removed during the adaptation (except base classes).

### Required Software, Tools and Prerequisites

* **Java** version: **Oracle Java 17** and higher (Execute `java -version` in command line after installation)
* **Apache Maven** version: **3.9+** and higher (Execute `mvn -version` in command line after installation)<br/>
There is `"gradle"` branch in this TAS project, so if you prefer to work with Gradle - migration will be easy, just merge the branch into main.

### How to build the Gradle project and setup different test runs

* Open a terminal or command prompt
* Go to project's root
* Execute `mvn clean test` if you just want to run all test suites and don't want to care about anything else
* Execute `mvn clean test -DtestSuiteName=all_tests` to explicitly specify that all test suites run
* Execute `mvn clean test -DtestSuiteName=regression_tests` in order to run full regression suite
* In order to run tests in parallel mode set appropriate thread-count in command-line parameter `mvn clean test -DthreadCount=3`
* _Example: you need to run all GUI-tests in MS Edge in parallel (2 threads) and headless mode, using local env-parameters:_<br/>
  `mvn clean test -DtestSuiteName=all_gui_tests -DthreadCount=2 -Dgui.browser=edge -Dheadless=true -Denv=local`

### Local run and debugging
There are three ways to run tests locally:
1) Using [▶] (Run Test button) directly above test method or test class (example: `src/test/java/com/taf/testscenarios/api/user/UserAPITest.java`)
2) Using one of XML TestNG suite from suites directory (example: `src/main/resources/suites/all_tests.xml`)
3) Using Maven command (example: `mvn clean install`)

Recommend to using the latest version of IntelliJ IDEA Community Edition with next plugins: Gradle, SonarLint, Lombok, TestNG, etc.<br/>

### API testing
API testing implemented through the package java.net.http, Google Gson and Jayway jsonPath<br/>
Execute `mvn clean test -DtestSuiteName=all_api_tests` in order to run only all API tests

TAF class returning instance of JavaNetClient calling method `getRestClient()`.

Method Chaining pattern is applied which means that calling different methods in a single line instead of calling other
methods with the same object reference separately. Under this pattern, we have to write the object reference once and
then call the methods by separating them with a dot.

### Mock API calls
If you need to develop API tests on endpoints that are still under development and are not available, but the contracts are known,
then you can start writing and debugging tests using a request interception server.<br>ServerInterceptorCalls class uses [WireMockServer](https://wiremock.org/) framework.<br>
**How to mock API test**: just extends from MockedBaseAPITest class in your test class and add required mock endpoint method, which should be called before sending a request <br/>
(***important note**: request URL have to contain address to localhost (by default) instead of real base URL or only endpoint part).<hr />

### GUI testing
WEB GUI testing is occurs through [Playwright](https://playwright.dev/java/) framework.<br/>
Execute `mvn clean test -DtestSuiteName=all_gui_tests` in order to run only all WEB GUI tests in primary (default) browser.

#### Image testing
For an image testing ImageHelper class was implemented.<br/>
It is powered by Sikuli library and implements two methods as examples:
1) The method works with Screen object.<br/>The method attempts to finding the part of screen with expected image.
2) The method works with Finder object.<br/>The method compares two (expected / prepared and actual / downloaded) images.

#### Cross-browser testing
For Google Chrome, Microsoft Edge and other Chromium-based browsers, by default, Playwright uses open source Chromium builds.<br/>
Using the default Playwright configuration with the latest Chromium is a good idea most of the time. Since Playwright is ahead of Stable channels for the browsers, it gives peace of mind that the upcoming Google Chrome or Microsoft Edge releases will display SUT correctly. You catch breakage early and have a lot of time to fix it before the official Chrome update.<br/>
Playwright can run tests on Chromium, WebKit and Firefox browsers as well as branded browsers such as Google Chrome and Microsoft Edge. It can also run on emulated tablet and mobile devices.<br/>

Collect the browser statistic of the SUT to define the required scope of browsers, or just use [Global Browser Stats](https://gs.statcounter.com/) (maybe one primary browser is enough in the case).
<br/>If XBrowser Testing is required this TAS is supported next browsers:
- **Chromium** (default);
- Google **Chrome**, using Blink engine developed as part of the Chromium project;
- Microsoft **Edge**, also using Blink engine;
- Mozilla **Firefox**, using Gecko engine;
- Apple **Safari**, using WebKit engine;

>This TAS skeleton prepared to realize both strategy of XBrowser Testing:
>- **The first**: All GUI tests are XBrowser, so each GUI test runs multiple times for each browser from the defined scope.
>- **The second**: All tests are divided into two types: first is a common type and that tests need to be run only once per
   testing session (in primary browser), and the second type of tests (prone to Cross-browser bugs) which need to be run multiple times for each browser from the defined scope.
>
>>An example of how possible to build CI/CD process in case of the **first XBrowser Testing strategy**:<br/>
>>Setup sequential jobs or stages of pipeline, for example the first run can be `mvn clean test -DtestSuiteName=all_api_tests` (it will run all API tests),<br/>
then `mvn clean test -DtestSuiteName=all_gui_tests -Dgui.browser=chromium` (it will run all GUI tests in Chromium (possible to skip browser parameter in this case)),<br/>
then `mvn clean test -DtestSuiteName=all_gui_tests -Dgui.browser=chrome` (it will run all GUI tests in Chrome),<br/>
then `mvn clean test -DtestSuiteName=all_gui_tests -Dgui.browser=safari` (it will run all GUI tests in Safari - WebKit based browser),<br/>
then `mvn clean test -DtestSuiteName=all_gui_tests -Dgui.browser=edge` (it will run all GUI tests in Edge),<br/>
after that `mvn clean test -DtestSuiteName=all_gui_tests -Dgui.browser=firefox` (it will run all GUI tests in Firefox (Nightly)).
>
>>In case of the **second XBrowser Testing strategy** necessary to define `groups = "XBrowser"` in parameter of `@Test` for all GUI tests which need to be run multiple times for each browser from the defined scope.
<br/>Then possible to build CI/CD process somehow like this:<br/>
>>Setup job or pipeline using the next command: `mvn clean test` (it will run all API and all GUI tests in Chromium (cover scope of Google Chrome and MS Edge)),<br/>
then `mvn clean test -DtestSuiteName=cross_browser_tests -Dgui.browser=safari` (it will run all GUI tests which marked "XBrowser" in WebKit based browser),<br/>
and finally `mvn clean test -DtestSuiteName=cross_browser_tests -Dgui.browser=firefox` (it will run all GUI tests which marked "XBrowser" in Firefox (Nightly)),<br/>

#### Mobile-view testing
* Execute `mvn clean test -DtestSuiteName=mobile_view_tests -Dgui.browser=android` in order to run all the UI tests that are marked for mobile view testing in Android emulation mode.<br/>
* Execute `mvn clean test -DtestSuiteName=mobile_view_tests -Dgui.browser=ios` in order to run all the UI tests that are marked for mobile view testing in iOS emulation mode.<br/>

Also, possible to use all GUI test suite in emulation mode:<br/>
`mvn clean test -DtestSuiteName=all_gui_tests -Dgui.browser=android`<br/>
`mvn clean test -DtestSuiteName=all_gui_tests -Dgui.browser=ios`,<br/>
but in this case, all the methods of the page object classes should be flexible and adaptable both for desktop and mobile view mode testing.<br/>
The size of the browser will take from the GUI constants class (`IOS_BROWSER_SIZE` and `ANDROID_BROWSER_SIZE`) that receives a value from TAF Properties (`gui.[emulated_device]BrowserSize`)<br/>
It's necessary to define `groups = "MobileView"` in parameter of `@Test` for all GUI tests which need to be run in a mobile view emulation mode as separate test suite.
<hr />

### Reporting

* **[Extent Reports](https://www.extentreports.com/)** framework is applied.

The report is generated after all the tests run in **Report.html**, and the location path is taken from the TAF properties.<br/>
**Project name** and the **system under test** displayed in the reports also taken from TAF properties.

To include the test to the Report you need to call the appropriate method for each test-method or placed it into `@BeforeMethod`.<br/><br/>
`@Test` annotation important parameters:
1. [x] `suiteName` parameter is used for grouping tests under a category or if this parameter is absent tests will group under "Other" category in the report.
2. [x] `testName` parameter is used for identify the test. If this parameter is absent test will get `[Absent name]`.
3. [x] `description` parameter is used for additional information (requirements links, tasks, id, etc.) and present in detail-headers of tests.

* To show method (step) invocation in the report details necessary to call the appropriate method each time for each target (action, assert, etc.) method.<br/>
  `showMethodInvocationInReportDetails` method takes boolean `showAsStep` to print in two possible styles (as method call or in human-readable style).<br/>
* Screenshots attached into details for failed GUI tests in Base64.

#### Tracing
Possible to attach a zip file with Playwright Tracing instead of a stacktrace log and a screenshot into details of a failed test.<br/>
To run all GUI test with collecting Playwright Tracing use appropriate system property `mvn clean test -DtestSuiteName=all_gui_tests -Dtracing=true`<br/>
Tracing system property is "false" by default, otherwise Playwright Tracing was enabled download a zip file and open it in [Playwright Trace Viewer](https://trace.playwright.dev/)<br/>
Playwright Trace Viewer hase large variety of functions, screenshots of each step, metadata, network calls, actual DOM structure, etc. which maces debugging much easier.<br/>
<hr />

### CI/CD

#### Continuous Integration implemented via [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)

There is `.gitlab-ci.yml` in the project root directory, which describes pipeline with some common AT jobs:
- Manual run AT job with flexible variables (variables dropdown with presets are available on GitLab -> CI/CD -> Run pipeline)
- Regression tests suite job launched on merge requests
- Daily night complete regression job (CI/CD schedule must be configured using cron and variable `REGRESSION_TYPE` with value `"daily"` on GitLab)
- Weekly night full regression jobs with all API tests, All UI tests in default browser, cross browser tests and mobile view tests (CI/CD schedule must be configured using cron and variable `REGRESSION_TYPE` with value `"weekly"` on GitLab)

The developers of Playwright prepared the Docker image `image: mcr.microsoft.com/playwright/java:v1.35.0-jammy`, which contains Java, Maven, Chromium, WebKit and Nightly.<br>
If in your case you need to set up and make registration of own [GitLab Runner](https://docs.gitlab.com/runner/#gitlab-runner) - just do all required work for registration and remove the image from `.gitlab-ci.yml`<br>

Job artifacts contains Report.html provided by Extent Reports, also `Running after_script` prints a direct link to the report in the job-log.
Besides in manual run possible to enabled saving of Playwright Trace instead of a stacktrace log and a screenshot into details of a failed test,
so zip-files with tracing also will be exported as part of job-artifacts and available to download and analyzing in Trace viewer.<br/>